﻿using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;
using Patterns;

public class PlayAudio : StatePatterns
{
    public float Duration { get; set; } = 0.1f;

    private float deltaTime = 0f;
    private SplashScreen m_splash;

    public PlayAudio(FSM fsm, SplashScreen splash) : base(fsm)
    {
        m_splash = splash;
    }

    public override void Enter()
    {
        deltaTime = Time.deltaTime;
        m_splash.GetComponent<AudioSource>().PlayOneShot(m_splash.audioLogo);
        base.Enter();
        Debug.Log("Entering: PlayAudio State");
    }

    public override void Update()
    {
        deltaTime += Time.deltaTime;
        if (deltaTime > 1f)
        {
            int nextId = (int)SplashScreen.SplashStates.FADE_OUT;

            StatePatterns nextState = m_fsm.GetState(nextId);
            m_fsm.SetCurrentState(nextState);
        }
    }
}
